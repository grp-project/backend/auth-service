import os
from pathlib import Path
from pydantic import BaseSettings, validator, MongoDsn, Field


BASE_DIR = Path(os.path.dirname(os.path.dirname(__file__)))
PROJECT_NAME = "GRP-Auth"

class Base(BaseSettings):
    class Config:
        env_file = os.path.join(BASE_DIR.parent, '.env')


class SettingsMongo(Base):
    protocol: str = "mongodb"
    username: str = Field(..., env="MONGO_INITDB_ROOT_USERNAME")
    password: str = Field(..., env="MONGO_INITDB_ROOT_PASSWORD")
    host: str = '127.0.0.1'
    port: int = 27017
    path: str = 'admin'
    db_name: str
    dns: MongoDsn = None

    @validator("dns", pre=True)
    def build_dsn(cls, v, values) -> str:
        protocol = values['protocol']
        username = values['username']
        password = values['password']
        host = values['host']
        port = values['port']
        path = values['path']

        return f"{protocol}://{username}:{password}@{host}:{port}/{path}"

    class Config:
        env_prefix = "MONGO_"


class UvicornSettings(BaseSettings):
    app: str = "main:app"
    host: str = "0.0.0.0"
    port: int = 8000
    reload: bool = False
    workers: int = 3

    class Config:
        env_prefix = "UVICORN_"


class Settings(Base):
    base_dir: Path = BASE_DIR
    project_name: str = PROJECT_NAME

    mongo: SettingsMongo = SettingsMongo()
    uvicorn: UvicornSettings = UvicornSettings()

