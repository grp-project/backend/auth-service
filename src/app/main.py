from fastapi import FastAPI
from beanie import init_beanie
# from routes.auth import router as AuthRouter
# from routes.mail import router as MailRouter
# from routes.register import router as RegisterRouter
# from routes.user import router as UserRouter
from motor.motor_asyncio import AsyncIOMotorClient
from models.user import User
from core import settings

app = FastAPI(
    title = settings.project_name,
    docs_url='/api/docs',
    openapi_url='/api/openapi.json',
    #default_response_class = ORJSONResponse,
)

@app.on_event("startup")
async def startup():
    """Initialize application services"""
    app.db = AsyncIOMotorClient(settings.mongo.dns).account
    await init_beanie(app.db, document_models=[User])


@app.on_event("shutdown")
async def shutdown():
    pass

# app.include_router(AuthRouter)
# app.include_router(MailRouter)
# app.include_router(RegisterRouter)
# app.include_router(UserRouter)
